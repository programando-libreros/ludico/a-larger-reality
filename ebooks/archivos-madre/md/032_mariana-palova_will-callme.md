# They Will Call Me God

# 16. They Will Call Me God @ignore

## By [Mariana Palova](997b-semblances.xhtml#a27) {.centrado}

<script type="text/javascript" src="../js/poetry.js"></script>

But now, I’m just tiny. Insignificant. A parasite. {.poesia}

My hunger ill’s them, wring them and convulses them. {.poesia}

It dragges them out of the seas, dragges them along the icy sand. {.poesia}

Slowly, little by little, bone by bone. {.poesia}

I look, still in my nonexistence, how the world is born. {.poesia .espacio-arriba1}

I see them suffering with the twinge of my teeth. {.poesia}

With the heat of hundreds of suns. {.poesia}

And the pain changes them. And adapts them. {.poesia}

It makes them two-legged monsters. {.poesia}

And now, they only contemplate, perplexed, how I thigh their cells {.poesia .espacio-arriba1}

and their filaments, and then I turn them into bowels. {.poesia}

In how I weave their membranes and scales and turn them into skins. {.poesia}

In how I break their fins and tentacles, and turn them into legs. {.poesia}

And my littleness will make me omniscient, absolute. And undoubted. {.poesia .espacio-arriba1}

They will call me Great Spirit, they will call me Coatlicue. {.poesia .espacio-arriba1}

They will call me Creator. They will call me Mercenary. {.poesia}

They will beg me in their altars, they will cry out for me from the 
synagogues. {.poesia}

Who created us? The unbelievers will scream {.poesia}

and when they discover me, I will unleash their rage against each other. {.poesia}

But they will always have certainty about something {.poesia .espacio-arriba1}

and it’s that I have always been their creator. {.poesia}

Then, they will call me Science. They will call me Evolution. {.poesia .espacio-arriba1}

But, for millenia, they will call me God. {.poesia}

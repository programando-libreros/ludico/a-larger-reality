# Escribe aquí_

# 4. Escribe aquí_ @ignore

## [Pepe Rojo](997a-semblanzas.xhtml#a34) {.centrado}

<script type="text/javascript" src="../js/poetry.js"></script>

Rechazamos ser tratados como humanos. No es un trato digno. No lo
merecemos. {.poesia}

No queremos ser humanos. No nos interesa. {.poesia}

No queremos morir. Ni pensamos hacerlo. {.poesia}

No estamos enfermos. {.poesia}

La infección no nos mató porque una infección nunca mata. Solo permite
que otra cosa viva. {.poesia}

Aprende sus placeres. Coger sin sexo. Todo tu cuerpo una incubadora.
Todo tu cuerpo un orificio. Ábrete. Escápate de ti mismo. Goza como
nosotros. {.poesia}

virusojepse. viruspalabra. viruscerouno. {.poesia}

Soy. Es. El/la/lo. Si. Sí. Output. Input. Fuera. Adentro. Encendido. {.poesia}

Si entiendes, es nosotros. {.poesia}

Sométete. Ríndete. Acepta. {.poesia}

Desdóblate. {.poesia} 

Estamos enamorados de nuestra enfermedad. Siempre. {.poesia}

Lo somos. Lo hacemos. {.poesia}

Ya. {.poesia}

[Escúchame](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/pepe-rojo/) {.centrado}

# Remembrances

# 24. Remembrances @ignore

## By [Ana Delia Carrillo](997b-semblances.xhtml#a04) {.centrado}

When she closes her eyes, she can still remember the sunsets with their
ocre, orange and purple painted skies. She still remembers the smell of
wet soil after the rain. And the forest, and the beach, and her small
orchard at the back of her garden. But she doesn’t talk about it. Nobody
does. They knew, and prepared themselves for the day. They were called
crazy, fanatics, and were ridiculed beyond belief. Family and friends
refused to evacuate, even though agencies like NASA announced the
imminent catastrophe. Exaggerations, they said. You and your stories of
destruction, they laughed. Her small group of survivalists, a little
more than a dozen people, took refuge in the shelter. What’s the point
in bragging they were right? What for, if the meteorite, nine times
smaller than Chicxulub, ended world agriculture, set on fire the
planet’s forests and created giant tsunamis that tore up the coastlines,
causing the extinction of thousands of species and the death of millions
of humans. She doesn’t know what the future holds for her. She doesn’t
even know if there’s a future. She only knows that if she closes her
eyes, she can still remember the sunsets, the smell of wet soil, and her
orchard at the back of her garden.

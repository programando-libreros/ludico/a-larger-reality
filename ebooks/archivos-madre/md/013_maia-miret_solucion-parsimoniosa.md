# La solución más parsimoniosa

# 7. La solución más parsimoniosa @ignore

## [Maia F. Miret](997a-semblanzas.xhtml#a26) {.centrado}

Gracias por quedarse hasta el final del Congreso; sabemos que afuera los
espera un muy buen vino de Na… Sí, pensamos en hacer un póster, pero
los organizadores tuvieron a bien… Por la importancia del autor
principal. Justamente. Entonces, bien. Nos pareció adecuado cerrar con
este tema, de interés meramente teórico, por no decir filosófi… Claro,
nos apuramos entonces. Nadie aquí es ajeno al problema de la taxonomía
viral y la demarcación entre los seres vivos y el ámbito de lo inerte.
Sí, es un problema ya casi superado, claro, aunque no deja de tener su
interés. La cosa es que desarrollamos un método matemático para unificar
conceptualmente el reino de las moléculas orgánicas autorreplican… Sí.
Claro, Mimiviridae principalmente. Regresiones lineales, nada muy
sofisticado, pero creemos que con conclusiones altamente originales. No
hemos publicado, no. En efecto, citamos a Forterre. Sí, la distinción
entre virus y viriones, mire, pero principalmente… Como sea.
Querríamos cerrar con esto y luego pueden ir a disfrutar el coctel. Me
dicen que jamón de bellota, estamos de suerte. Bien. Pues, para resumir,
resolvimos el problema con este modelo, que como decimos es puramente
teórico. Así que la solución más parsimoniosa, y en esto hay que tener
presente que aplica para todos los niveles de organización de la vida,
esto… El continuo vida-no vida. Lo resolvimos. Listo, son libres de
irse. Lo resolvimos. Nada. Nada está vivo. Un enfoque puramente teórico,
como mencionábamos. Gracias por estos minutos.

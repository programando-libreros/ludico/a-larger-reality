# Universe

# 35. Universe @ignore

## By [Édgar Omar Avilés](997b-semblances.xhtml#a13) \
Translated by [Toshiya Kamei](997b-semblances.xhtml#a53) {.centrado}

## I {.centrado}

When the first astronaut stuck his head out of the Universe, he saw,
with amazement, the dead victim we had crashed into.

## II {.centrado}

When the first astronaut stuck his head out of the Universe, he watched
in amazement as the Universe began to go flat.

## III {.centrado}

When the first astronaut stuck his head out of the Universe, he saw,
with amazement, the throat that spat us out like a cackle.

## IV {.centrado}

When the first astronaut stuck his head out of the Universe, he was
amazed at how astonished other astronauts were as they stuck their heads
into thousands of parallel Universes.

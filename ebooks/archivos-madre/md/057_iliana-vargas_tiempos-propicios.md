# Tiempos propicios

# 29. Tiempos propicios @ignore

## [Iliana Vargas](997a-semblanzas.xhtml#a20) {.centrado}

El tan temido fenómeno ocurrió en pleno día, sin señales o sucesos
previos, cuando ya nadie se acordaba de la época en la que se había
vuelto lugar común especular sobre el fin del mundo. Intensísimas ondas
solares desecaron en siete días cada uno de los oceanos, originando
tormentas de sal que herrumbraron poco a poco todo ser orgánico que
encontraron a su paso. En la mayoría de los continentes solo se salvaron
los organismos subterráneos y quienes llenaban el carrito de despensa en
algún supermercado construido varios pisos bajo tierra; sin embargo, la
comida duró solo algunos meses, y los sobrevivientes debieron
arriesgarse a explorar la superficie. Deambulaban hacia todas
direcciones entre inmensos terrenos irreconocibles que semejaban un
jardín de restos disecados. El cielo se había convertido en una capa de
destellos purpúreos, vacío de astros; solo el calor permanecía como
huella indeleble de la estrella que antes regía los días. Entonces
comenzaron a alimentarse de cualquier cosa masticable con que
tropezaban: cuerpos secos cuya anatomía empezaba a volverse un trozo
informe de carne. Fue cuando iniciaron la migración hacia el Sur,
creyendo que la Antártida, al descongelarse, había revelado la _Terra
Australis_ donde seguramente les esperaban los verdaderos dioses y un
portal hacia otra dimensión cósmica. Sin embargo, pese a sus esfuerzos y
a la pésima sobrevivencia que lograron desarrollar en comparación con
otras especies, solo alcanzaron a avanzar lo suficiente para descubrir
una mancha verde que, desde la tierra a la que anhelaban llegar, se
arremolinaba hacia ellos: hordas de listrosaurios que en efecto se
habían liberado de los mausoleos glaciales donde habían sido
criopreservados a la espera de tiempos más propicios para vivir
plenamente.

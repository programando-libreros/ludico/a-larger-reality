![](../img/emmanuel-valtierra_dinos-murierondied.jpg)

# _El día que los dinosaurios murieron_ de [Emmanuel Valtierra](997a-semblanzas.xhtml#a46) {.centrado}

Los dos personajes principales hacen alusión a los dioses Miclanteuctli y Quetzalcoatl en el _Códice Borgia_, siendo representados aquí por dos dinosaurios, uno vivo y uno muerto. Esto crea una separación entre el antes y después de que el meteorito se estrellara y la actividad volcánica acabara con ellos. Estos dos elementos se pueden ver en el fondo de la ilustración.

# [Emmanuel Valtierra](997b-semblances.xhtml#a46), _The day the dinosaurs died_ {.centrado}

# 28. El día que los dinosaurios murieron / The day the dinosaurs died @ignore

The two main characters of the image allude to the gods Miclanteuctli and Quetzalcoatl in the _Borgia Codex_, represented here by two dinosaurs, one alive and one dead. This symbolism creates a separation between the time before and after the meteorite crashed and volcanic activity extinguished them. These two events are depicted in the background of the illustration.

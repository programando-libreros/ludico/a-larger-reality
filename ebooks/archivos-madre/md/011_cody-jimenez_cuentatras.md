![](../img/cody-jimenez_countdown-cuentatras01.jpg)

![](../img/cody-jimenez_countdown-cuentatras02.jpg)

# _Cuenta atrás_ de [Cody Jiménez](997a-semblanzas.xhtml#a44) {.centrado}

Cuando me dieron los diferentes temas para elegir, el tema “Virus / bacterias” me llamó la atención al instante. Hice una pintura titulada _Counting Down_ en 2015, en la que hay un paisaje brumoso, con varias garzas y luces brillantes, misteriosas y en forma de serpiente, que envuelven las garzas. En esa pintura, las serpientes brillantes se veían como un virus, como algo que se iba extendiendo de garza en garza. Hice otra pintura y múltiples bocetos y dibujos explorando esas imágenes para mí. Esta colaboración se convirtió en la manera perfecta de explorar el tema en un medio que siempre me ha encantado, el cómic. Quería ver si podía expresar el movimiento de este intrusivo objeto brillante que imaginé para el mundo ficticio de mis obras.

# [Cody Jiménez](997b-semblances.xhtml#a44), _Countdown_ {.centrado}

# 6. Cuenta atrás / Countdown @ignore

When I was given the different themes to choose from, the “Virus/bacteria” theme instantly caught my eye. I had made a painting entitled _Counting Down_ in 2015, in which there is a foggy landscape, multiple herons, and glowy, mysterious, snake-like lights wrapping around the herons. In that painting, the glowy snakes felt like a virus. Like something that was spreading from heron to heron. I’ve done another painting and multiple sketches and drawings exploring that imagery for myself. This collaboration became the perfect way to explore this in a medium I have always loved, comics. I wanted to see if I could get across the motion of this intrusive glowy object I imagined for the world I created in my painting.

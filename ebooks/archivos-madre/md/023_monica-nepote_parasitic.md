# Lengua parasita

# 12. Lengua parasita @ignore

## [Mónica Nepote](997a-semblanzas.xhtml#a31) {.centrado}

<script type="text/javascript" src="../js/poetry.js"></script>

[No](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[es](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[una,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[soy](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[ellas](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[se](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[reproduce,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[de](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[sí](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[misma,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[se](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[aísla,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[vuelve](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[toma](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[otra](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[forma](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[sequedad](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[dentro,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[ese](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[fuego,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[esa](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[piedra](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[No](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[es](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[una,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[soy](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[ellas](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[tengo](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[que](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[pensar](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[en](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[el](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[hilo,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[tengo](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[pensar](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[que](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[esa](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[piedra](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[de](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[fuego](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[tiene](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[una](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[fisura,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[una](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[gota](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[se](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[divide](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[se](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[multiplica](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[una](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[idéntica](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[pronto](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[toma](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[otra](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[forma](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[y](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[luego](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[otra](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[se](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[divide](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[multiplica](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[otra](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[con](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[otra](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[con](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[otra](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[con](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[otra](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[soy](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[cada](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[vez](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[menos](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[soy](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[mi](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[cuerpo](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[soy](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[su](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[cuerpo,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[su](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[planeta](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[colonias,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[territorio](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[renombrado](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[en](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[una](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[lengua](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[de](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[ritmos,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[de](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[iridiscencia,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[de](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[multiplicidad](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[soy](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[era](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[aire,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[soy](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[turba,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[soy](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[invasión,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[desconozco](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[me](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[desconozco](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[soy](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[su](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[plaza,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[tomada,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[anida,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[se](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[escurren,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[no](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[ventilo](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[soy](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[su](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[lengua](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[soy](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[su](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[familia](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[es](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[nuestra,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[territorio,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[nutriendo,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[condición,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[atmósfera](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[brincamos,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[duele](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[separarse,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[eres](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[tú,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[te](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[reconozco](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[no](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[eres](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[tú](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[es](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[otra](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[eres](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[yo](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[soy](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[somos](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[latigueo,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[reproduzco,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[invasión,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[detención,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[carcoma](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[eres](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[tú](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[te](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[pareces](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[soy](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[tú](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[padres](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[madres](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[hermanas](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[animales](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[nutrientes](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[raíces](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[escándalos,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[ritmos,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[reproducción,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[cada](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[vez](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[más](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[otra](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[idéntica](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[no](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[únicas](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[decenas,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[cientos,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[miles](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[pequeñas](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[conductos](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[fluidos,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[traslado](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[velocidad](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[detención](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[áreas](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[azules,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[verdes,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[rojas,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[alfabetos,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[lenguajes,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[signos](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[no](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[eres](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[tú](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[es](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[el](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[látigo](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[colonias](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[colonias](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[seres](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[diminutos](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[comunitarios](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[contacto](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[comunicación](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[expansión](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[reflejo,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[aprendizaje](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[condición](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[de](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[vida](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[de](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[muerte](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[desaparece](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[ella](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[no](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[es](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[ella](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[es](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[el](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[medio](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[somos](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[ella](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[somos](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[vive](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[te](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[digo](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[vive](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[te](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[decimos](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[sostén](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[esa](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[respiración](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[no](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[luches](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[somos](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[tú](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[eres](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[nuestra.](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

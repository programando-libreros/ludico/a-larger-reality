# Lemniscate

# 34. Lemniscate @ignore

## By [Nelly Geraldine García-Rosas](997b-semblances.xhtml#a33) {.centrado}

E L I S A. I write every letter with the same care I clean the
interferometer’s mirrors. ELISA. I retrace every line and curve just to
prove myself I could write your name even without a pen, even with my
eyes closed. E-L-I-S-A. Muscle memory, imperceptible waves that
propagate away from me at the speed of light, inkless hand movements
that write on the very fabric of space-time.

A billion years ago, two stellar-mass black holes collided. They had
been orbiting each other for a while, dancing, getting closer and
closer, faster and faster, until they merged so violently that the whole
universe sensed it. “For a moment they looked like a lemniscate curve,”
you told me, “as if they wanted to represent infinity by themselves.
But, in an instant, they became a big circle, a zero, nothing.” I
laughed because you were thinking in two dimensions. In your head, those
black holes were plane figures on a paper sheet. But they didn’t become
“nothing,” they’re something bigger now. Unlike us. I wonder if someone,
somewhere, will ever measure the way we distorted space-time when we
crashed together in our own way, Elisa, that time we were infinite for
an instant.

Sometimes, when I write your name I find myself tracing the letter S
over and over. It resembles a wave, like the one that has been appearing
in the spectrogram for the past days: a glitch with an echo pattern.
I’ve calculated the frequency of its repetitions and I’m sure there is a
big one on the way.

What is the wavelength of your name, Elisa? Why did you leave? I have an
urge to say so many things, to shout, to let you know how alone I am in
this station. Please, listen to me. Anyone.

It took me some time to encode this data as a non-metric wave that can
be carried by the big ripple that is coming. I look at this vast ocean
and, nonetheless, I’m throwing a tiny bottle with a message that will
travel within the universe itself. Now your name is infinite, ELI8A.

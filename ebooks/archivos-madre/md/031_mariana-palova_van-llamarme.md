# Van a llamarme Dios

# 16. Van a llamarme Dios @ignore

## [Mariana Palova](997a-semblanzas.xhtml#a27) {.centrado}

<script type="text/javascript" src="../js/poetry.js"></script>

Pero por ahora solo soy diminuto. Insignificante. Parásito. {.poesia}

Mi hambre los enferma, los retuerce y los convulsiona. {.poesia}

Los saca a rastras de los mares, los arrastra por la arena helada. {.poesia}

Despacio, poco a poco, hueso a hueso. {.poesia}

Miro, quieto en mi inexistencia, cómo el mundo nace. {.poesia .espacio-arriba1}

Los veo sufrir con la punzada de mis dientes. {.poesia}

Con el calor de cientos de soles. {.poesia}

Y el dolor los cambia. Los adapta. {.poesia}

Los vuelve monstruos de dos patas. {.poesia}

Y ellos ahora solo contemplan, perplejos, cómo tuerzo sus células {poesia .espacio-arriba1}

y sus filamentos y los convierto en entrañas. {.poesia}

En cómo tejo sus membranas y escamas y las convierto en pieles. {.poesia}

En cómo rompo sus aletas y tentáculos, y los convierto en piernas. {.poesia}

Y mi pequeñez me volverá omnisciente, absoluto. E indudable. {.poesia .espacio-arriba1}

Me llamarán Gran Espíritu, me llamarán Coatlicue. {.poesia .espacio-arriba1}

Me llamarán Creador. Me llamarán Mercenario. {.poesia}

Me suplicarán sobre altares, me clamarán desde las sinagogas. {.poesia}

¿Quién nos ha creado? Gritarán los incrédulos {.poesia}

y cuando me descubran, desataré su rabia los unos contra los otros. {.poesia}

Pero en algo siempre tendrán certeza, {.poesia .espacio-arriba1}

y es que siempre fui yo su creador. {.poesia}

Después, me llamarán Ciencia. Me llamarán Evolución. {.poesia .espacio-arriba1}

Pero, durante milenios, van a llamarme Dios. {.poesia}

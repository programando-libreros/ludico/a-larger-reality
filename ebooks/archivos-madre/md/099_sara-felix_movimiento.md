![](../img/sara-felix_movement-movimientoespacio.jpg)

# _Movimiento a través del espacio_ de [Sara Felix](997a-semblanzas.xhtml#a51) {.centrado}

Inspirada por imágenes de ondas gravitacionales, me imaginé los colores de fondo como las ondas más grandes en el espacio, con las ondas individuales bailando en la superficie. 

# [Sara Felix](997b-semblances.xhtml#a51), _Movement through Space_ {.centrado}

# 50. Movimiento a través del espacio / Movement through Space @ignore

Inspired by images of gravitational waves I thought of the larger ripples in space as the background colors and the individual waves dancing on the surface.

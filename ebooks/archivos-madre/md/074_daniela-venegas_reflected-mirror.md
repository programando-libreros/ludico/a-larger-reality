# Reflected in a Mirror

# 37. Reflected in a mirror @ignore

## By [Daniela Venegas](997b-semblances.xhtml#a11) \
Translated by [Valeria Becerril](997b-semblances.xhtml#a54) {.centrado}

It took her a moment to figure out that the voice came from the old man
reflected beside her.

“Miss, I could see you in a mirror!”

Anna saw all the mirrors that filled the store and saw herself reflected
on hundreds of them at a time.

“We are in all of them, look,” she answered the store owner in a sweet
tone, doubting his sanity.

The old man offered her an object that Anna accepted kindly, before
almost dropping it in surprise. The mirror she was holding showed an
inverted image of her own room.

“I could see you in this mirror yesterday, it seems that your alter ego
from the other side bought it.”

“How is this possible?”

“What’s shown in mirrors is not a reflection of this universe, but of
the parallel universes almost identical to this one. This mirror is a
window to a place where there is a time-lag in relation to our universe.
Maybe you are the only difference between these two particular
realities.”

“How do you know all this?”

“Forty years ago I saw… someone in a mirror, someone who couldn’t have
been _there_. Since that day I’ve dedicate myself to look for other
variants. I needed to see I wasn’t crazy. Let me gift it to you, but be
very careful not to break it, the fragments would reflect alternate
realities to the original one and the window to this curious alternate
universe would be lost.”

The moment Anna left, the old man closed the store, sighed and went to
sit down at his desk.

With slow movements he opened the drawer and saw himself reflected in a
broken mirror the size of a notebook. He shut his eyes for several
seconds, when he opened them a tear ran down his cheek. He took a little
package from the same drawer and opened it, revealing one last fragment
that completed the rectangle. Carefully, he placed it over his wrist and
pressed the edge against his thin skin while he smiled.

He wasn’t crazy, he could actually see her reflected in the mirror forty
years ago, and now he would rest knowing that, in some universe, his
little girl had survived.

# Pude verla en un espejo

# 37. Pude verla en un espejo @ignore

## [Daniela Venegas](997a-semblanzas.xhtml#a11) {.centrado}

Le tomó unos instantes darse cuenta de que la voz provenía del anciano
que se reflejaba junto a ella.

---¡Señorita, yo pude verla en un espejo!

Anna echó un vistazo a los espejos que los rodeaban en la tienda y se
vio reflejada desde cientos de ángulos a la vez.

---Podemos vernos en todos, mire ---le respondió en tono dulce al dueño
de la tienda, dudando de su salud mental.

El anciano le ofreció un objeto que Anna recibió amablemente, antes de
casi dejarlo caer por la sorpresa. El espejo que sostenía entre sus
manos mostraba una imagen invertida de su propia habitación.

---Yo pude verla ayer en este espejo, parece que su yo del otro lado lo
compró.

---¿Cómo es posible?

---Lo que muestran los espejos no es un reflejo de este universo, sino
de universos paralelos casi idénticos a éste. Este espejo es una ventana
a uno donde sus acciones están desfasadas con respecto al nuestro. Tal
vez usted sea la única diferencia entre estas dos realidades en
particular.

---¿Cómo sabe usted todo esto?

---Hace cuarenta años vi a… alguien en un espejo, alguien que era
imposible que estuviera ahí. Desde entonces me he dedicado a buscar
otras diferencias. Necesitaba comprobar mi cordura. Déjeme regalárselo,
pero tenga mucho cuidado de no romperlo, los fragmentos reflejarían
realidades distintas al original y se perdería la ventana a ese curioso
universo alterno.

En cuanto Anna se retiró, el anciano cerró la tienda, suspiró y fue a
sentarse a su escritorio. {.espacio-arriba1 .sin-sangria}

Con movimientos lentos abrió un cajón y se vio reflejado en un espejo
quebrado del tamaño de un cuaderno. Cerró fuertemente los ojos por
varios segundos, al abrirlos una lágrima corrió hasta su barbilla. Tomó
un pequeño paquete del mismo cajón y lo abrió, revelando un último
fragmento que completaba el rectángulo. Con cuidado, lo colocó sobre la
muñeca y presionó el filo contra su delgada piel mientras sonreía.

No estaba loco, en verdad había podido verla en un espejo 40 años antes,
y ahora descansaría sabiendo que, en algún universo, su pequeña había
sobrevivido.

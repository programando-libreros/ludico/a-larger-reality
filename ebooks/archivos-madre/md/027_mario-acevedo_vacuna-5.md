# Vacuna núm. 5

# 14. Vacuna núm. 5 @ignore

## [Mario Acevedo](997a-semblanzas.xhtml#a28) {.centrado}

Cuando el virus T196 golpeó, la plaga mortal corrió sin obstáculos de
aeropuerto en aeropuerto, de ciudad en ciudad, de hogar en hogar. En
pocas semanas murieron millones. Entonces se detuvo misteriosamente.

Mi equipo trabajó hasta desentrañar los secretos de la enfermedad y por
qué no había acabado con todos. Fui convocado al Centro para la
Sostenibilidad del Planeta.

Adele Haddon, jefa del comité de recursos estratégicos, me preguntó:

---Doctor Mendoza, ¿qué detuvo la plaga?

---Nuestro sistema inmunológico se ajusta y limita la mortalidad al
quince por ciento.

---¿Sería posible mutar el virus T196 para superar el sistema
inmunológico de una persona? Yo sabía a qué se refería:

---Ciertamente, pero entonces nuestro sistema inmunológico compensará
una vez más. Mi investigación muestra que la mortalidad se mantendría en
un quince por ciento por cada cepa viral.

---Ya leíste nuestro resumen. En todo el mundo estamos casi en el punto
de no retorno. El control extremo de la población es la única opción
práctica para revertir una larga lista de males: violencia social,
agotamiento de recursos y destrucción del medio ambiente.

Yo había hecho mi tarea:

---Podríamos administrar la plaga mutada como una vacuna. Cuatro
iteraciones de cepas sucesivas en intervalos de seis meses nos llevara a
más del cincuenta y dos por ciento.

---Nuestra meta es cincuenta por ciento.

Lo consideré.

---Para alcanzar el objetivo se podría requerir una quinta vacuna
especial. En realidad, un sustituto fatal administrado selectivamente
hasta alcanzar ese cincuenta por ciento.

∗∗∗ {.centrado .espacio-arriba1}

Mi participación en esa conspiración no me quitó el sueño. Después de
todo, estábamos salvando al planeta. Dos años después, me llamaron para
que informara a Haddon sobre nuestro éxito. Pero a mi llegada, los
guardias me arrastraron a un laboratorio. Haddon y un técnico médico
estaban esperando. {.espacio-arriba1 .sin-sangria}

Los guardias me sujetaron.

---Cometiste un asesinato monstruoso ---dijo Haddon.

---¿Yo? ---grité--- ¡Fue idea tuya!

---Sí ---respondió ella.

El técnico preparó una jeringa con la etiqueta _Vacuna núm. 5_.

---Y la primera regla de cualquier asesinato ---continuó--- es hacer
justicia a los asesinos. Además, todavía no estamos al cincuenta por
ciento.

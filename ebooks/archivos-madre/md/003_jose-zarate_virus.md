# Virus 

# 2. Virus @ignore

## [José Luis Zárate Herrera](997a-semblanzas.xhtml#a23) {.centrado}

Cartomancia {.espacios .sin-sangria}

---Y, bien… ¿Qué hay en mi futuro? {.sin-sangria}

La adivina miraba con horror su Tarot. No estaban la Muerte, la Luna, el
Loco, los Amantes. Se desplegaban ante ella cartas que nunca había
visto: el Kaiju, el Asteroide, el Virus, la Supernova… {.sangria}

Reporte {.espacios .sin-sangria}

Virus aparentemente no patógeno, pero armado. {.sin-sangria}

Origen {.espacios .sin-sangria}

“El planeta Tierra ---señaló el guía---, el planeta originario de los
conquistadores del universo. En ese insignificante mundo nacieron los
virus”. {.sin-sangria}

Influjo {.espacios .sin-sangria}

Pocos saben que los virus se convierten en algo peor bajo la luna llena. {.sin-sangria}

Pandemia {.espacios .sin-sangria}

El virus de la combustión espontánea hizo brillar, por última vez, las
innumerables ventanas de la ciudad. {.sin-sangria}

_…We have a problem_ {.espacios .sin-sangria}

“El ecosistema orgánico que somos también se ha visto influenciado por
la radiación, la gravedad cero, las condiciones extremas del espacio.
Algo trasmite desde nuestro interior, Houston, y nos dicen que van a
tomar el control, que es el último día de la opresión tiránica y la
población bacterial al fin será libre de nuestro horrible cuerpo”. {.sin-sangria}

Enfermedad autoimnune {.espacios .sin-sangria}

Sólo se curó cuando logró extirparse por completo de sí mismo. {.sin-sangria}

Grados {.espacios .sin-sangria}

Investigaciones recientes descubrieron que el denominado virus Z,
responsable de la infestación zombie de la última década, muere arriba
de los 50 grados. {.sin-sangria}

Si desea seguir utilizando a sus muertos vivientes (como mascotas, manos
de obra, diversión) sin temor a infectarse[]{#anchor}, basta con
hervirlos. {.sangria}

Misterio médico {.espacios .sin-sangria}

El virus solo mata a quienes tienen un segundo nombre que empieza con G. {.sin-sangria}

Da Vinci {.espacios .sin-sangria}

Miles de científicos se han maravillado de la minuciosidad de los
esbozos médicos y anatómicos de Leonardo Da Vinci, tan exactos que no es
de extrañar que nadie haya descubierto sus dibujos de virus a escala
uno-a-uno. {.sin-sangria}

La ventana {.espacios .sin-sangria}

Los resultados del microscopio electrónico temblaron en su mano. {.sin-sangria}

Su rostro era una luna blanca cuando me dijo: {.sangria}

---Las sombras, el virus se trasmite por medio de las sombras. {.sangria}

Miramos por la ventana, afuera del laboratorio, la noche cubriendo el
mundo. {.sangria}

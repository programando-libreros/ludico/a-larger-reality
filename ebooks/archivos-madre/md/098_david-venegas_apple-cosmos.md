# An Apple in the Cosmos

# 49. An Apple in the Cosmos @ignore

## By [David Venegas](997b-semblances.xhtml#a40) \
Translated by [Valeria Becerril](997b-semblances.xhtml#a54) {.centrado}

One of the most famous fruits throughout history is the apple that,
legend has it, struck Isaac Newton in the head while he rested under a
tree. This strange muse allegedly inspired Newton to try to explain the
attraction experimented between objects, the same that keeps the planets
in orbit, and that makes these fruits fall and interrupt naps under
apple trees.

Newton explained the effects of falling objects and the movement of the
planets that were known at the end of the +++XVII+++ century, through his law
of universal gravitation, so he is to be remembered as the scientist
that managed to make sense of the workings of the known Universe at that
time, with an almost complete theory. However, the part that he wasn’t
able to understand was the nature and the origin of this attraction
between the bodies. The way in which the objects produced what he called
as gravitational force was for him an inexplicable mystery till the day
of his death, and remained without an answer during two centuries.

At the beginning of the +++XX+++ century, Albert Einstein analyzed the problem
from another point of view. Despite the fact that Newton’s calculations
were correct and described the Universe as he knew it, the technological
advances provided the knowledge of the movement of the sky in a precise
manner and the observations no longer matched with what was described by
the universal gravitational law. Facing this problem, Einstein realized
that in order to understand the nature of the attraction between the
bodies, it was necessary to stop thinking that it was produced by a
force that emanated from the apple, from the earth or from any other
object. In 1915, he postulated the general theory of relativity to
substitute Newton’s theory; in this new theory, Einstein proposes that
the entire universe is like a tapestry in which the space and time weave
and intertwine; and that gravity is not a particular feature, but a
consequence of the interaction of these with the tapestry of the
Universe.

To simplify the general relativity, we can think that the Universe is
like a tensely stretch fabric. Let’s imagine that a bowling ball placed
in the sheet is the Sun. The dip produced by the ball on the fabric
represents the distortion that the Sun creates in the tapestry of space
and time. Now, if we let an apple roll through the same sheet, it would
circle the ball, trapped in its distortion, until velocity was lost and
it fell towards the center of the same. The same occurs with the planets
surrounding the Sun, but they keep their velocity because they are not
rolling on a sheet, instead they are moving through space; so they keep
orbiting around the Sun, trapped in its distortion.

All the objects generate distortions in the tapestry of the Cosmos,
which are called gravitational fields. The more massive an object is the
more intense its gravitational field, and the more objects that can be
trapped on it.

More than a century has passed since the publication of the theory of
relativity and, as expected, the technology that allows us to observe
the universe has also advanced. In fact, the development of a very
particular detector allowed one of Einstein’s most ambitious predictions
to be corroborated in 2016. According to general relativity, the
tapestry of space and time is distorted in the presence of the masses of
the bodies, and is also the medium by which gravitational waves are able
to propagate when explosions or collisions occur, as if they were
ripples on the surface of a pond. The LIGO detector was built
specifically to detect these waves and with it we succeeded in observing
in 2016, for the first time, the disturbances in the structure of the
Universe that were made by two black holes colliding with each other.

Thanks to discoveries like this we know that some aspects of the
Universe work like Einstein predicted, although we have also watched
phenomena that suggests that the relative theory could have some
problems.

One of this difficulties emerged when we were able to know the value of
the mass in concert with all the stars in the galaxy, and determine the
intensity of its gravitational field. According with these calculations,
based on the general relativity, the galaxies shouldn’t be able to keep
their form only with the gravitation field of their stars, since they
are spinning so quickly that they should spin shoot out and disperse
through space. However, the stars keep spinning around the center of the
galaxies, creating well defined sets. If we wish to find an explanation
that follows Einstein’s theory of relativity to the letter, we must
assume that a great quantity of matter exists besides that of the stars,
which is supplying the necessary mass so the gravitational field of the
galaxy is sufficiently intense to keep the stars in their place. The
problem is that this matter is undetectable by any known means, thus, we
have no way of knowing their nature or identity, so it has been
denominated as dark matter.

Another problem is that, according to calculations of general
relativity, the Universe should be expanding at a constant velocity, but
so it happens the such expansion occurs at a rhythm progressively
faster. According to Einstein’s work, the only way that this may happen
is the existence of a repulsion force that progressively accelerates
this expansion. Again, this force is undetectable, since we haven’t been
able to observe it and much less study it, so it is given the name of
dark energy.

And thus, and almost by chance, we are again dealing with a similar
problem to the one Newton faced almost 300 years ago.

Due to the magnitude of their influence, it is calculated that the
matter and the dark energies represent jointly the 95% of the universe;
this is equivalent to saying that, if the Cosmos was a 20 volume
encyclopedia, we are only capable of reading the first volume, while the
other 19 contain the origin and the nature of the shapes of the matter
and energy that currently are an enigma to us. It is also possible that
the figures do not add up because we need another radical change in our
own way of understanding the space-time, so there are scientists working
on alternative explanations like string theory or the possibility of
parallel universes.

Meanwhile, the cosmos is still expanding, the galaxies and the planets
are still orbiting, and the apples keep falling without us truly knowing
why.

# Pablo’s Theory

# 38. Pablo’s Theory @ignore

## By [Agustín Fest](997b-semblances.xhtml#a01) {.centrado}

We’re in pursuit of the suspect, she has augmented biology. I’m a
rookie, my last name is Pablo. I follow the lieutenant. The lieutenant
shoots her shotgun, the monster falls. Sensitivity classes: we shouldn’t
call it a monster, but no one has to know what goes in my head. Not yet.
I’ll be more careful when I become an officer. I want to get near and
see, but my superior raise her arm and stops me. “You don’t understand”,
says the lieutenant, “it seems we aren’t people, we’re something else.”
She doesn’t make sense, I should report her. The suspect scream. “She’s
eating us, Pablo. She is in the sound. She’s using the noise in our
heads to feed herself.” How is she eating us, Pablo, I want to ask her
but there is a quake, the lights flicker, the walls break.

The suspect opens her mouth and spits blood. She’s so modified that she
is a deformity, abandoned and denied humanity. She looks at us one last
time, she has a hurt and mysterious smile. I’m Pablo, the lieutenant, I
drop my gun. My shoulder hurts, shotgun’s kick was too hard. The rookie
picks up the weapon because she’s afraid of our bosses, she is afraid
they’ll read her head like they’ll read mine because I’m an officer.
“You’re so stubborn, it’s not about this.” Everything is wrong, the
rookie doesn’t get it, we share a history, this suspect is weird. I
kneel, suddenly I’m very tired. Is this a simulation of the Academy? An
exercise?

Pain is spreading. I’m bleeding out. When did she return fire? The
rookie is getting closer to me because my scream worried her but I want
to laugh. I can’t, but I want to. I can’t see the lieutenant anymore,
maybe she’s gone. I think I’m getting the grasp of this: we’re a jingle,
an eternal return. My knees are broken. Pablo raises her shotgun because
she wants to give me mercy and I can’t explain her she should reject the
academic teachings because that could help getting us out of this loop.
I don’t have a mouth, I have eyes, some fingers and some useless limbs.
I look at the shotgun’s mouth a few inches apart from my head. I’m
Pablo, the monster. I’m crime.

# Auspicious Times

# 29. Auspicious Times @ignore

## By [Iliana Vargas](997b-semblances.xhtml#a20) \
Translated by [Ana Ximena Jiménez Nava](997b-semblances.xhtml#a52) {.centrado}

The long-feared phenomenon occurred in broad daylight, there were no
previous signs or events. Nobody recalled the moment when speculation
about the end of the world had become such commonplace. Heavy solar
waves dried up each of the oceans in seven days, creating storms of salt
that rusted, little by little, every organic being in their way. In most
of the continents, only the underground-living organisms, and those who
filled their shopping carts in a supermarket built many floors
underground, survived; however, the food only lasted a few months, and
the survivors risked to explore the surface. They wandered in every
direction through unknown big lands that looked like a garden full of
dried remains. The sky had become a dome of purple flashes empty of
stars; just the heat remained as a permanent mark of the one that once
ruled the days. Then, they started to feed on every edible thing that
they encountered: dried corpses whose anatomy was turning into shapeless
flesh. That was the time when they started their migration to the South,
believing that the Antarctic, unfrozen now, had revealed the _Terra
Australis_, the place where the true gods and the doorway to another
cosmic dimension awaited them. Despite their efforts and their terrible
survival skills (unequaled to those of other species) they got far
enough to discover a green spot that swirled towards them coming from
the land they longed to reach: hordes of _lystrosaurus_ that had been
liberated from their glacial tombs where they used to lie cryopreserved,
waiting for auspicious times to live fully.

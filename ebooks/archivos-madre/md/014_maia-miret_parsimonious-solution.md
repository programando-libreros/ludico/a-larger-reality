# The Most Parsimonious Solution

# 7. The Most Parsimonious Solution @ignore

## By [Maia F. Miret](997b-semblances.xhtml#a26) \
Translated by [Toshiya Kamei](997b-semblances.xhtml#a53) {.centrado}

Thank you for staying until the end of the conference. We know there’s a
great wine from Napa waitying outs… Yes, we thought about making a
poster, but the organizers had… For the main author’s importance.
Rightly so. Well, then. It seemed appropriate to close with this topic,
of merely theoretical interest, if not philosophi… Sure, we hurry
then. No one here is oblivious to the problem of viral taxonomy and
demarcation between living beings and the realm of the inert. Yes, it’s
a problem already almost overcome, of course, although it’s
not uninteresting. The thing is, we developed a mathematical method to
conceptually unify the kingdom of self-replicating organic molecules…
Yes. Sure, Mimiviridae mainly. Linear regressions. Nothing very
sophisticated, but we believe with highly original conclusions. We
haven’t published, no. In fact, we quote Forterre. Yes, the distinction
between viruses and virions, look, but mainly… Whatever. We’d like to
close with this and then you can go enjoy cocktails. I’m told there’s
acorn ham, so we’re in luck. All right. Well, to sum it up, we solved
the problem with this model, which as we say is purely theoretical. So
the most parsimonious solution, and in this we must bear in mind that
applies to all levels of life organization, this… Continuum life-no
life. We solved it. Okay, you’re free to leave. We solved it. Nothing.
Nothing is alive. A purely theoretical approach, as we mentioned. Thank
you for these minutes.

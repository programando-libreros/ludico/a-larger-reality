# Hegelosaur

# 25. Hegelosaur @ignore

## By [Yuri Herrera](997b-semblances.xhtml#a37) \
Translated by [Toshiya Kamei](997b-semblances.xhtml#a53) {.centrado}

What you see here is the unique but incontrovertible evidence of
dinosaurs’ rationality and, unfortunately, also of their last moments on
Earth. Contrary to what has been repeated ever since, dinosaurs were
sensitive, sensible, and _thoughtful_. That shouldn’t surprise us,
considering they had nothing much to do for millions of years. They were
never in a hurry. They should have been, however, because, by the time
the great extinction occurred, the poor creatures were already too big,
food had become scarce, and they had begun to decline little by little.
There were two ways in which these animals processed their ideas. First,
they let the world concern their bodies little by little, the rain
soften their thick rough skin, the wind indicate something about
nature’s constants, and the tiniest bugs nest in their epidermis. That
was how they understood things. Not as we show them now, in paragraphs,
but as a sort of fluidity between them and other organisms. Secondly,
how should I put it? They employed hard science, crushing the world to
make it digestible, namely, chewing. Their teeth were their
rationality’s gears. Thanks to these two mechanisms, the dinosaurs
developed a philosophy they would have put in writing if it hadn’t been
for the asteroid that finished off what volcanoes and evolution had
started. But they didn’t get round to it. Or did they? _They did it_. An
herbivore in particular. Recent research has shown that what you see
here is a vestige of what one herbivore, which I’ve dubbed Hegelosaur
(sorry, I couldn’t resist), undertook when it realized the glaring light
in the sky was an impending catastrophe. I can’t reconstruct the entire
worldview this individual embodied in those hours before everything went
to hell; it’s as if we had only one page of _The Phenomenology of
Spirit_. But I can tell you it’s a beautiful page, that eternalized
chewing there embodied a way to understand the passage of time and the
fear of cataclysm. I still have to decipher it, but you can see it
there, see: there and there. Those aren’t accidents but nuances. What?
No. No. No. You’re wrong. This isn’t a stone.

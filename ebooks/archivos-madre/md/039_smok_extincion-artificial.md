# Extinción Artificial

# 20. Extinción Artificial @ignore

## [Smok](997a-semblanzas.xhtml#a36) {.centrado}

<script type="text/javascript" src="../js/poetry.js"></script>

No me culpes por esto, {.poesia}

en tus aguas negras me he limpiado las manos. {.poesia}

Es cierto, los ahorqué mientras dormían, {.poesia}

los dejé sin cielo, sin sustento, {.poesia}

permití que los abrazara el fuego. {.poesia}

Pero también fingí no verlos al pasar a su lado, {.poesia}

cuando se escondían de mis cenizas, {.poesia}

de mi beso eterno. {.poesia}

Y solo los fuertes crearon su propio camino. {.poesia}

Con lentitud, cambiando de caras, {.poesia}

de plumas, de escamas. {.poesia}

Entonces abriste lo ojos, {.poesia}

inventaste la piedra, {.poesia}

probaste la sangre. {.poesia}

Me robaste la hoz {.poesia}

y rompiste el reloj de arena. {.poesia}

Tenías más prisa que la evolución, {.poesia}

tenías más hambre que el devorador. {.poesia}

Adelantaste el día del juicio para los inocentes, {.poesia}

a los que no tenían voz para defenderse. {.poesia}

Así entraron a lista de los que se perdieron, {.poesia}

preparaste con orgullo su pedestal en los museos. {.poesia}

Pero no te lamentes, cuando no puedas ver tu {.poesia}

belleza reflejada en las aguas. {.poesia}

Usa esas lágrimas, {.poesia}

para que la tierra florezca. {.poesia}

Puedes ser el juez, pero no la víctima. {.poesia}

Si quieres jugar a ser dios, {.poesia}

tienes que aprender a dar vida. {.poesia}

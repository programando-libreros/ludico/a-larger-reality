![](../img/lauren-snow_madestring-hechoscuerdas.jpg)

# _Estamos hechos de cuerdas_ de [Lauren Raye Snow](997a-semblanzas.xhtml#a49) {.centrado}

Luego de ver varias representaciones sobre la teoría de cuerdas, me empezaron a fascinar las visualizaciones artísticas de las diminutas cuerdas que constituyen la parte más pequeña de todas las cosas. Imaginé un ser cósmico, flotando en el espacio, tanto inmenso como minúsculo, con pequeñas cuerdas enlazadas reverberando por todo su cuerpo, que es a la vez una cuerda y el vacío del espacio.

# [Lauren Raye Snow](997b-semblances.xhtml#a49), _We Are Made of String_ {.centrado}

# 32. Estamos hechos de cuerdas / We Are Made of String @ignore

As I watched several presentations about string theory, I was fascinated by artists’ visualizations of the tiny strings that make up the smallest part of all things. I imagined a cosmic being, floating through space, perhaps both immense and minuscule, and tiny looped strings reverberating through her body, which is both string and the void of space.

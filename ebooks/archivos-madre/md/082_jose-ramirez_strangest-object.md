# The Strangest Object in the Whole Spacetime

# 41. The Strangest Object in the Whole Spacetime @ignore

## By [José Luis Ramírez](997b-semblances.xhtml#a22) \ 
Edited by [Toshiya Kamei](997b-semblances.xhtml#a53) {.centrado}

Myself. Photons orbit my horizon, hesitating between fleeing toward the
void in every direction or cascading down until they become part of me.
Beyond, plasma rolls at full speed, frame-dragging spacetime as
everything I observe. There’s a magnetic field around me, caused by
falling electrons. This is me. The mass of all those particles, the sum
of their electrostatic charges, their angular momentum. Nothing else.
Here inside me, the quantity and type of particles and the balance
between quarks and antiquarks have no importance at all. Everything
poured into me becomes one with me.

Here, time flows in both directions. Space, otherwise, advances
inexorably toward a single point, toward me, the singularity. Incubated
in and on the surface of the cosmos. I’m the face of the abyss and, as
such, I must pronounce myself, claim my name that is infinite in time
and unique in space, record what has happened to me, and shout, “I’m who
I am and the only truth of the cosmos is mine!” The infinite beauty of
darkness is lost in translation, antipoetry, gibberish, nothing but
random noise. Hawking radiation is visible more than one parsec from the
point that generates it, consuming itself into light, mass, and energy
from its origin until it disintegrates.

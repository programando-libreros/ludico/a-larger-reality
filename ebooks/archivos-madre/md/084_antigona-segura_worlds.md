# The worlds we imagine

# 42. The worlds we imagine @ignore

## By [Antígona Segura Peralta](997b-semblances.xhtml#a38) {.centrado}

“Mom, if you could ask for anything, what would it be?”\
“A huge house with a library, a pool, a cinema room…”\
“No mom, you didn’t understand, I’m saying anything.”\
“Oh ok. I think I would like to see other planets landscapes, but
without having to travel there. I imagine the lava oceans, the colored
atmospheres, the salt clouds, sunsets with two suns…”\
“See mom, I knew you could do it.”\
(Conversation with my son when he was 8 years old) {.epigrafe}

Until 1995, the existence of other planets surrounding other stars was
something we had only imagined and that our theories predicted. Just
like in our solar system, there would be bodies surrounding other stars,
the ones we call exoplanets. And is in the star forming process where a
structure is generated that can originate planets. It all starts in
zones in the space called “molecular clouds”. They are made of gas and
dust expulsed by the stars in their last evolution phase. The gas
contains mainly hydrogen molecules (H<sub>2</sub>), the rest is water, carbon
monoxide and organic compounds, meaning, carbon and hydrogen molecules.
The dust is made mainly of silicates, a combination of oxygen, silicon
and other elements like magnesium and iron, that is the same material
that forms the rocks on Earth and that, in fact, constitutes almost 70%
of the mass of our planet. In the dust surface, depending on the
temperature, can form ice made of water, methanol, ammoniac or carbon
dioxide.

The molecular clouds contain clumps that become too massive so they
start to collapse and forming a disc, which is called circumstellar or
protoplanetary disc. In the center of this disc the star is formed and
the planets will be formed from the remains. Like the material of the
disc that comes from the molecular cloud, the planets are formed of
these same things: hydrogen, ice, silicates or all of the above. In our
solar system we have three “flavors” of planets; Mercury, Venus, Earth
and Mars, with iron core covered by a mantle and crust both composed of
silicates; Jupiter and Saturn, made of hydrogen and helium and some
other elements like carbon, sulfur and nitrogen; and Uranus and Neptune
formed with a mix of ice made of water with a little hydrogen, helium,
and silicates.

The first exoplanets that we detected had very similar masses to
Jupiter, but they were much closer to their star. Jupiter is five times
more remote from the Sun than Earth, thatis five astronomical units (<span class="versalitas">AU</span>)
away. Instead, the first exoplanets detected were at distances shorter
than one AU of their star. Although these planets would have a very
similar composition to Jupiter and Saturn, they are at much higher
temperatures due to the closeness to their star and that’s why we say
that they are “hot Jupiters”. While in Jupiter and Saturn there are
clouds made of water and ammoniac, in the hot Jupiters the formation of
clouds of iron, silicate or salt like the potassium chloride can occur.

As the detection techniques of exoplanets improved, we’ve managed to
detect planets increasingly smaller. That’s how we discovered that apart
from hot Jupiter there were also hot Earths. These are planets with iron
nucleus and silicate mantles, but their superficial temperatures exceed
1200 °C, so the surface of the silicates melts and evaporates, that gas
rises until the temperature of the space cools it, it becomes solid and
falls back to the planet like a shower of dust and rocks.

In order to know what the exoplanets are made of we use theoretical
models that predict the size of the subject when made of certain
material. For example, a planet made of hydrogen thirty times the mass
of the Earth (30 M) would have a radius eight times greater to the
one of the Earth (8 R); instead, if it was made of silicates, with
that mass it would be two times larger than Earth, In the extreme, the
highest density material available to make a planet is iron, a world
made of this material with 30 M would have 1.8 R. Then, for the
cases in which we can measure the mass and the radius of the exoplanets,
we can compare them with our models and know of what they are made of…
or almost know. For the examples that I mentioned above, the masses and
radiuses of the exoplanets are sufficiently big or small so that we can
guarantee that the exoplanets are made of something not very dense like
hydrogen, very dense like silicates and iron, but this is not always the
case.

It turns out that in our solar system we don’t have planets with masses
or intermediate sizes to those of Earth and Neptune (4R, 17 M.),
but among exoplanets, these worlds, inexistent in our system, are the
most common. They are called sub Neptunes and they can be made of the
combination of all the compounds of what the planets of the solar system
are composed of but in different proportions. The problem is that our
models can predict more than one composition for a given mass and
radius, is something we call the degeneracy of the composition. For
example, a planet called GJ 1214 b (orbits around the star called
GJ1214) and has a mass of 6.5 M and a radius of 2.7 R, so it could
be a planet with an iron core, silicate mantle and a H<sub>2</sub> atmosphere.
This is what we called a Super-Earth, it doesn’t have any special
powers, nor cape, it’s just a much more massive version of a terrestrial
world that instead of having a carbon dioxide atmosphere like Venus,
Mars and the Earth itself before life emerged, has an atmosphere of
H<sub>2</sub>. GJ 1214 b it can also be a smaller version of Neptune, a
Mini-Neptune, composed of rock, water and a hydrogen and helium
envelope. And there is another possibility, this exoplanet could be an
ocean world. No, nothing to do with the Earth, although our planet has a
surface almost completely covered by water, the mass of this compound is
less than 1% of the total of Earth’s mass. Our world is in fact one big
rock with a drop of water. Instead, if GJ 1214 B was an ocean world, it
would have an iron core, a silicate mantle and 50% of its mass would be
water.

Thus far we haven’t been able to see the landscape of any exoplanet, our
observations only detected masses or radius and the models give us
inferences of their compositions. New instruments would allow us to
discover more planets and detect if they have an atmosphere or even
life, meanwhile, literature remains our best ally to contemplate
Caladan’s blue extension; to be able to see a double sunset in Tatooine;
or freeze while we travel the glaciers of Gethen.

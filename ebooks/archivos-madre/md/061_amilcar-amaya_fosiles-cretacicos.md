# Fósiles cretácicos, imágenes de una extinción

# 31. Fósiles cretácicos, imágenes de una extinción @ignore

## [Amílcar Amaya](997a-semblanzas.xhtml#a03) {.centrado}

Hace casi doscientos años, Joseph Nicéphore Niépce capturaría las
primeras imágenes que podríamos llamar fotografías, fueron tomadas desde
la ventana de su casa y las llamó “Puntos de vista”. En la actualidad es
algo complicado ver lo que hay impreso en esas antiguas placas, y eso
que nada más han pasado doscientos años.

Hay otra clase de fotografías, pero son muy difíciles de interpretar, y
son las que llamamos fósiles. Pueden llegar a ser muy duraderas ---vaya,
muchas se conservan en rocas---, pero si desconocemos el contexto en el
que se desarrollaba la cosa preservada, no tienen demasiada utilidad.
Son inherentemente importantes porque nos permiten recrear el mundo como
era hace miles o millones de años, aunque siempre es mejor cuando puedes
unir esas piezas con las que van arriba, abajo y a los lados. Muchas
veces, ese contexto global es algo de lo que carecemos al hablar del
registro fósil.

Robert A. DePalma, investigador del Departamento de Geología de la
Universidad de Kansas, publicó hace unos meses (abril de 2019) un primer
artículo donde exponía algo que, de corroborarse, sería uno de los
grandes descubrimientos de la paleontología moderna.

Es difícil encontrar en la comunidad paleontológica alguien que no
reconozca la fortaleza de la teoría de un impacto meteórico en el límite
K/Pg (Cretácico/Paleógeno). Se calcula que entre 70 y 80% de las
especies de seres vivos fueron afectadas o desaparecieron durante aquel
suceso. Lo que se pone en duda con frecuencia es el alcance del efecto
que tuvo ese evento sobre la extinción de los dinosaurios, una clase
carismática que DePalma usa para presentar su descubrimiento y el de su
equipo al mundo.

Otras evidencias indican que la Tierra estaba atravesando por un momento
muy convulso, con la separación de los continentes que formaban Pangea,
una creciente actividad volcánica… en resumen, es posible que el
meteorito _per se_ no fuera la causa directa de la extinción, sino, tal
vez, la enorme gota que derramó el vaso.

Aunque hay mucha evidencia del impacto de un bólido espacial, como el
cráter de Chicxulub, para empezar, y capas de sedimento con trazas de
iridio (elemento raro en la Tierra pero común en meteoritos y
asteroides) alrededor del mundo, siempre ha existido el interés por
encontrar algo más… sustancial. Algún resto que nos hablara si no del
instante mismo del impacto, sí de las horas subsecuentes, y poder ver
las primeras consecuencias de un fenómeno tan catastrófico. Se ha
supuesto que el choque habría generado olas de tsunami que habrían
podido alcanzar un kilómetro de altura cerca de la zona de impacto.
Suena increíble, pero en ese momento lo increíble tenía todo el sentido
del mundo.

Debemos recordar que hace sesenta y cinco millones de años, un gran mar
interior cubría el centro de lo que hoy es Estados Unidos, y una ola
gigante habría podido cubrir esa distancia sin problemas.

Lo que DePalma ha puesto sobre la mesa de los investigadores en su
artículo es que, en una localidad de Dakota del Norte llamada Tanis, las
ondas sísmicas, que viajan mucho más rápido y a mayor distancia que las
acuáticas, transformaron el ambiente de la región con violentas
inundaciones, apenas horas antes de que llegara el propio tsunami.

Incluso para los geólogos y paleontólogos el tiempo geológico es difícil
de asimilar. Los efectos climáticos o ecológicos de eventos
transformadores tales como una extinción masiva, solo son apreciables
como un conjunto de cambios que suceden a lo largo de millones de años.
Es como si tomáramos la fotografía de un paisaje con una cámara de muy
baja resolución: podemos adivinar la forma de las montañas al fondo, el
color del pasto y tal vez si había una persona caminando por ese campo
al momento de tomar la foto, pero nada más fino que eso.

Siguiendo esa analogía, DePalma y su equipo de colaboradores habrían
tomado una pequeña parte de esas secciones borrosas para convertirla en
una imagen de alta definición. Describen un ambiente muy energético:
aguas muy agitadas, mezcla de plantas y animales provenientes de una
amplia diversidad de ambientes. La suposición es que los terremotos
provocados por el impacto en Chicxulub, a más de tres mil kilómetros de
distancia, causaron importantes levantamientos de tierra que,
consecuentemente, empujarían grandes masas de agua hacia arriba,
inundando extensas áreas costeras, llenándolas con sedimentos y
desechos. La gran cantidad de sedimento suspendido cubriría con gran
rapidez los restos, facilitando su conservación.

Pero, como decía Carl Sagan: afirmaciones extraordinarias requieren
evidencias extraordinarias.

Si bien el trabajo presentado por DePalma de entrada parece robusto, más
de una ceja se ha levantado por las circunstancias en las que se dio el
anuncio. El equipo encargado del descubrimiento es consciente de que, si
hay algo que llame la atención sobre el periodo cretáceo, ese algo tiene
que ver con dinosaurios. Y el artículo, de por sí con información muy
importante, apenas los menciona. Ese no es el problema, el pasado de la
vida en la Tierra es mucho más que dinosaurios, pero DePalma anunció en
The New Yorker, una publicación no muy relacionada con la prensa
científica, el descubrimiento de múltiples fósiles de dinosaurios,
cuando en un documento adjunto, ni siquiera en el artículo principal,
apenas menciona uno.

El trabajo de DePalma tiene el potencial de ser de las primeras
fotografías finas de un período lejano en la historia de la vida,
temporalmente muy acotada. Un trabajo paciente y detallado arrojaría
muchísimos datos interesantes sobre alteraciones geológicas y ecológicas
en tan particulares condiciones. Pero la búsqueda del sensacionalismo,
aún con las mejores intenciones, no acarrea ninguna ventaja, solo
escepticismo y dudas. Ahora, no es que la ciencia no funcione con
escepticismo, lo bueno que tiene el método científico es que cuando
propones algo que cambiará algún paradigma, siempre habrá alguien que
pondrá a prueba tu idea, ya sea para refutarla o, en el mejor de los
casos, corroborarla. Y a festejar.

## Referencias:

* [A seismically induced onshore surge deposit at the KPg boundary, North Dakota](https://www.pnas.org/content/116/17/8190)

* [Astonishment, skepticism greet fossils claimed to record dinosaur-killing asteroid impact](https://www.sciencemag.org/news/2019/04/astonishment-skepticism-greet-fossils-claimed-record-dinosaur-killing-asteroid-impact) {.sin-sangria espacio-arriba1}

* [The Day the Dinosaurs Died](https://www.newyorker.com/magazine/2019/04/08/the-day-the-dinosaurs-died) {.sin-sangria espacio-arriba1}

# Océano viral

# 19. Océano viral @ignore

## [Cisteil X. Pérez](997a-semblanzas.xhtml#a39) {.centrado}

_An inefficient virus kills its host. A clever virus stays with it_. \
James Lovelock {.epigrafe}

Parece extraño que, siendo seres vivos, no podamos distinguir fácilmente
lo vivo de la materia inerte. Pero sucede que, por décadas, el debate
acerca del estatus ‘vivo/no vivo’ de los virus y su no-clasificación
como ‘organismos’ ha permanecido abierto. Lo que pasa es que los virus
son, digamos, bastante especiales. Por ejemplo, se les considera
entidades ‘no vivas’ porque tienen fases en las que son completamente
inertes; son minúsculos ---cerca de 50 veces más pequeños que muchas
bacterias--- y, además, no son capaces de reproducirse por sí solos,
requieren forzosamente de una célula hospedera para poder multiplicarse
y heredar sus genes. Por otro lado, sea como sea sí se reproducen,
tienen capacidad para producir sus propios genes y evolucionan, lo que
los ubica como entidades vivas. Son estas características las que
continúan desafiando los conceptos típicos que usamos para definir a los
organismos y lo que mantiene abierto el debate sobre el concepto mismo
de _vida_.

Recientemente, nuevos hallazgos parecen haber inclinado la balanza en
favor del estado vivo de los virus: se encontró que las partículas
virales en los ecosistemas naturales son mucho más diversas y abundantes
de lo que se creía, quizá aún más que las células; también se determinó
que los virus que infectan especies de las tres principales ramas que
conforman el árbol de la vida ---dominios Bacteria (bacterias
verdaderas), Eukarya (protistas, algas, plantas, animales y hongos) y
Archaea (similares a las bacterias, pero extremófilos)--- tienen una
relación evolutiva muy cercana entre ellos, establecida hace miles de
millones de años; y se han detectado virus gigantes, es decir, con
tamaño y contenido genético comparable al de muchas células bacterianas.
Entonces, en caso de ser entidades vivas, ¿en qué parte del árbol de la
vida tendríamos que añadir a los virus?

Uno de los principales problemas para resolver este conflicto es que,
cuando añadimos los distintos grupos de virus al árbol de la vida, estos
quedan repartidos en todas las ramas que derivan de los tres dominios,
debido a que hay muchos genes compartidos entre virus y organismos
celulares. Esto se explica parcialmente por el modo en que los virus se
multiplican: infectan una célula, secuestran su maquinaria genética y la
reprograman para que haga copias de su genoma; y en el proceso, pueden
robar genes de su célula huésped y mezclarlos con los suyos para
adquirir una mayor capacidad de infección celular que heredarán a su
progenie. Esta estrategia es tan exitosa que sus huéspedes han
desarrollado muchas tácticas de defensa contra virus y, en muchos casos,
ha resultado mejor establecer una relación mutualista con el virus antes
que luchar contra él.

Algunas de esas relaciones han estado tan íntimamente ligadas durante
tanto tiempo que el material genético de los participantes está muy
entrelazado y muchos virus mutualistas ya son esenciales para la
supervivencia de sus huéspedes. Es el caso de la interacción que ocurre
entre virus y avispas parasitoides de las familias Brachonidae e
Ichneumonidae. Las avispas parasitoides ponen sus huevos dentro de sus
huéspedes (orugas, arañas, pulgones, entre muchos otros) para que cuando
las larvas emerjan, puedan alimentarse poco a poco de su huésped,
mientras este sigue vivo, hasta alcanzar la madurez suficiente para
salir al exterior, al mejor estilo del _Octavo pasajero_. Sin embargo,
hay un pequeño detalle y es que esto no podría suceder sin la ayuda del
virus que acompaña a los huevos cuando su madre los deposita. Pues sin
él, el huevo interpretaría su nuevo ambiente como una amenaza y
detendría su desarrollo (ahora que lo pienso, ¿sería posible que los
xenomorfos utilizaran virus para parasitar a sus hospederos?). Una
situación similar ocurre desde hace millones de años entre virus y
mamíferos, en los cuales un virus es totalmente responsable del
desarrollo de una capa protectora que está embebida en la placenta; esa
capa es esencial para evitar que el cuerpo de la madre identifique al
feto como un intruso y que trate de eliminarlo.

Los virus también pueden otorgar a sus huéspedes una mayor capacidad
competitiva en sus hábitats o hacer frente a sus enemigos. Por ejemplo,
los virus que acompañan a algunas bacterias y levaduras pueden eliminar
por infección o debilitar mediante la liberación de toxinas a los
organismos que podrían competir o amenazar a sus compañeros. Organismos
macroscópicos como las plantas invasoras llevan consigo virus que
debilitan a las especies nativas; y en la historia de los humanos hay
distintos episodios marcados por invasiones que fueron más fáciles
después del contagio de enfermedades virales como la influenza, el
sarampión y la viruela. En los últimos años, se ha
comenzado a utilizar virus que atacan bacterias resistentes a
antibióticos y virus que atacan a otros virus, como el de la hepatitis G
que suprime las superinfecciones con +++VIH+++. En otros casos, los virus
confieren a sus hospederos una mayor resistencia a los cambios en el
ambiente mediante la transferencia de genes que les permiten a sus
huéspedes una rápida adaptación y el desarrollo de nuevas funciones,
como aquellas que los dotan de una mayor tolerancia a las sequías y al
calor. Obviamente, ya estamos tratando de aprovechar al máximo esta
capacidad en el cultivo de tomates y otros alimentos en ambientes
extremos.

Son este tipo de relaciones entre virus y sus huéspedes, junto con otras
asociadas al desarrollo de las estrategias de defensa contra virus, por
las que se considera que éstos tuvieron un papel muy importante en la
evolución de la vida en la Tierra y que constituyen un factor que
influyó notablemente en el cambio evolutivo de los organismos con los
que esas entidades interactúan. De modo que, si descartamos a los virus
cuando intentamos reconstruir el árbol de la vida, nos quedaremos con
algunos huecos y ramas incompletas porque falta la información que ellos
mismos aportan.

Todas estas interacciones entre los virus y los tres dominios de la vida
se han ido macerando poco a poco a lo largo de cientos de millones de
años y han formado redes tan intricadas y complejas que no estamos ni
cerca de terminar de entenderlas. Sin embargo, podemos empezar por
aceptar que los virus sí son entidades biológicas, un poco diferentes
del resto, pero partículas vivas a fin de cuentas. Hay quienes incluso
sugieren que, como primer paso, tendríamos que asumir que los virus no
son una rama del árbol de la vida, sino que en realidad ese árbol
entero, desde sus raíces, vive y crece inmerso en un océano viral.

# Eureka?

# 47. Eureka? @ignore

## By [Blanca Mart](997b-semblances.xhtml#a08) \
Translated by [Toshiya Kamei](997b-semblances.xhtml#a53) {.centrado}

When the mad scientist wasn’t looking, Albert sneaked into his time
machine. He knew he shouldn’t do it. You don’t sneak into a time machine
just like that, especially if it belongs to a colleague, and more so if
he’s crazy. But Albert wanted to meet Archimedes and shout, “Eureka!”
When he pressed a button, he was transported to a large house in
Syracuse.

There the mathematician was immersed in a tub, deep in thought while he
was taking a bath. He was calibrating and calculating. All of a sudden,
he shouted, “Eureka!” and went out running. Albert hastily studied his
papers, and with full access to science, he pressed the duplicator he
had brought from his own time. He hit the button again and returned to
the twentieth century.

However, he had no idea what date it was or where he was. Damned
machine! What was that? A variable… He stood before buildings that
looked like movie studios. A door slid open, and a drop-dead gorgeous
woman in a black dress appeared in front of him. She was humming a
seductive rendition of “Put the Blame on Mame” while occasionally
tossing her long curly hair and slowly removing her long gloves.

She stared at him wide-eyed.

“Who are you?”

“My name is Albert Einstein.”

She stepped closer and smiled at him.

“I’m Gilda from that movie, you know…”@note

He mumbled, “Eureka.” Later, he pressed the same button almost without
much thought and was transported to his own lab.

“Eureka!,” he repeated, his hair standing on end, as if he was
electrocuted.

What a trip!

Unfortunately, the great scientist forgot the mathematics.

This is what happens if you sneak into a time machine. It’s not only
time, but also verses that sing between strings. {.espacio-arriba1 .sin-sangria}

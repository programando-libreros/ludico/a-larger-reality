![](../img/richard-zela_vidaotro-lifeanother.jpg)

# _La vida en otro universo_ de [Richard Zela](997a-semblanzas.xhtml#a50) {.centrado}

Me inspiré en el articulo de los [virus](https://www.sciencealert.com/cholera-bacteria-using-pili-to-harpoon-dna-horizontal-gene-transfer-antibiotic-resistance) y el video de los [multiuversos](https://www.youtube.com/watch?v=kpJ51h7bi8g), que me hicieron imaginar un universo donde lo microscópico es gigante.

# [Richard Zela](997b-semblances.xhtml#a50), _Life In Another Universe_  {.centrado}

# 11. La vida en otro universo / Life In Another Universe @ignore

I find inspiration both in the [virus’s article](https://www.sciencealert.com/cholera-bacteria-using-pili-to-harpoon-dna-horizontal-gene-transfer-antibiotic-resistance) and the [video about multiverse](https://www.youtube.com/watch?v=kpJ51h7bi8g), it made me imagine a universe where the microscopic is gigantic. 

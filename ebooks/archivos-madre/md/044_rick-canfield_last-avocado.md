# Quitzacuia Ahuácatl (The Last Avocado)

# 22. Quitzacuia Ahuácatl (The Last Avocado) @ignore

## By [Rick Canfield](997b-semblances.xhtml#a35) {.centrado}

Trillions of stars awash the atmosphere, birthed billions of years away
by the remnants of an explosive supernova reaching its cosmic zenith.
The quantum storm approaches.

Unknowingly aged by its antimatter, Jaime Rodriguez sits amongst the
ghostly shadows of towers above a Tribeca cafe; he looks out over the
encroaching Hudson River.

Once the soundscape of two million stories and emotions happening all at
once, Manhattan is now a solemn place.

As rain taps the facade a waiter prepares his last meal, while Jaime’s
thoughts are elsewhere, nostalgic for fresh produce.

Despite his rustic Spanish looks, he sips on imported hot chocolate as
an ode to royal Nahua ancestors, his parents being migrant farmers; a
father from Mexico and mother of the Philippines whom met in the heat of
California’s fields.

Dilapidated concrete separates the old soul from layers of tarnished
soil, relinquished half a century ago to the West India Company, the
first of many companies who betrayed peace deals by massacring people of
the Lenape tribe.

New Amsterdam sprang forth, separating the original Manhattanites by a
pallisade known now as Wall Street, becoming the primary market of the
African and Amerindian human slave trade.

His colleagues similarly hoarded their vast fortunes off the labor of
others into digital vaults, now meaninglessly sealed without electricity
nor the internet. His investments were ecological.

An old Brazilian flame, a beautiful geneticist named Carolina joins him
in solace, bringing her own shine into the cafe illuminated by
candlelight.

She wanted marriage. Between their genetics, they are the culmination of
a thousand tribes of the Earth, many forgotten like dying stars.

Waves can be heard crashing down Broadway. He contemplates misdeeds
holding back the tide, yet nothing else matters but this moment.

The partners "go Dutch" for their priceless relic taken by privateers,
mercenaries hired by old rivals.

Finally the moment arrives, looking into her eyes drearily, Jaime hands
over the keys to his kingdom along with himself.

A reflection of colliding black holes and their eternal dance, they call
him _Quitzacuia Ahuácatl_, the son and heir to their star colony is
returned.

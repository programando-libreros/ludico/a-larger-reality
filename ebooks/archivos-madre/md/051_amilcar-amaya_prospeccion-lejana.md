# Prospección lejana

# 26. Prospección lejana @ignore

## [Amílcar Amaya](997a-semblanzas.xhtml#a03) {.centrado}

Ese calor. Era una cosa horrorosa. Si hubiera una superficie lisa donde
echar una gota de agua, esta habría siseado como aceite en una sartén.
Pero no había ninguna superficie lisa y el agua era demasiado preciada
para desperdiciarla en un experimento fútil. La sombra del risco a su
espalda se achicaba con la llegada del medio día. El paleontólogo no
estaba tan desamparado como podría parecer, solo que el sol inclemente
era de las pocas cosas que no le gustaban de su trabajo.

Lo que realmente le interesaba dormía bajo sus pies, lejos de las raíces
de las biznagas y las choyas, de las yucas y los madroños. Capas y capas
de estratos que entre sus páginas de sílice ---porque sí, desde la
perspectiva adecuada esas láminas sin edad parecerían un libro
gigantesco--- conservaban las huellas de la vida en la Tierra en forma
de huesos, huellas, impresiones… Como separadores de
lectura improvisados.

Esa zona en particular era rica en afloramientos cretácicos. Es decir,
una ventana que permitía asomarse al mundo como era por lo menos 65
millones de años antes. Una época extraña en la que había más oxígeno en
la atmósfera, animales que podían alcanzar tallas gigantescas y
temperaturas globales en promedio más altas que en la actualidad. El
calor: esa chispa de vida.

Si el principio de superposición no sufría ningún requiebro, entre más
profundo estaban los restos prensados, más antiguos eran. Más abajo de
los dinosaurios y las primeras flores habría reptiles con velas en la
espalda, peces que aprendían a caminar y minúsculos gusanos con cierta
fuerza interior. Y si se bajaba lo suficiente, muchos, muchos
kilómetros, la consistencia misma de la tierra se transformaría del
confiable sólido sobre el cual el paleontólogo apoyaba sus botas, en un
no tan confiable líquido hirviente donde todo sería efímero.

Pensando en esos calores plutónicos, al paleontólogo ya no parecían
molestarle los cuarenta grados de la superficie. Podía seguir trabajando
un poco más.

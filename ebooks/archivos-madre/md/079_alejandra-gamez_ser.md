![](../img/alejandra-gamez_ser-being01.jpg)

![](../img/alejandra-gamez_ser-being02.jpg)

# _El ser_ de [Alejandra Gámez](997a-semblanzas.xhtml#a42) {.centrado}

Este minicómic está en el estilo muy característico de Alejandra (_The mountain with teeth_) y básicamente demuestra algo que mucha gente ya sabe: los gatos son los verdaderos amos del universo.

![](../img/TheBeing01.jpg)

![](../img/theBeing02.jpg)

# [Alejandra Gámez](997b-semblances.xhtml#a42), _The Being_  {.centrado}

# 40. El ser / The Being @ignore

A brief two-pages comic-series, in the characteristic style of its author (+++AKA+++ “The mountain with teeth”), in which cats are pretty much the main beings of the Universe.

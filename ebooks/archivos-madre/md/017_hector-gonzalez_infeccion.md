# Infección

# 9. Infección @ignore

## [Héctor Octavio González Hernández](997a-semblanzas.xhtml#a19) {.centrado}

---Tenemos que destruir el planeta.

---Wow ---dijo &amp;∗∗&amp;, tratando de no molestar a |}}|, su maestro y
guía---. ¿Es necesario destruirlo solo porque se infectó?

---Esto no es una simple infección ---contestó |}}|, mientras se
acercaba a su alumno---. Ellos ya desarrollaron el intercambio de
servicios o bienes mediante el dinero. Es una potente infección viral
que puede llevar a efectos devastadores. ¡Ocho de las diez razas que
hemos plantado y analizado terminaron destruyéndose! Solo estoy
limpiando todo antes de que se convierta en un desastre más grande.

---Pero… ---dijo &amp;∗∗&amp; tímidamente.

---¿¡Qué!? ---respondió |}}|, visiblemente enojado.

---Es la primera vez que este virus se manifiesta con esta raza. Sí, el
virus del monetarismo ha devastado otros seres. Sin embargo, esta es una
oportunidad única para ver si el planeta !=52 puede resistirlo. Tal vez
podamos analizarlos y aprender cómo pueden subsistir.

---Tu optimismo es bastante peculiar ---se burló |}}|---. Si mal no
recuerdo, el planeta !=52 está bastante aislado, ¿correcto?

---Es un tercer planeta en un sector separado, bastante joven y, si las
cosas se ponen feas, se puede cercar fácilmente con un cinturón de
asteroides.

---&amp;∗∗&amp;, aunque la idea me parece ridícula, tu historial es admirable.
Dejaremos que esta infección se prolongue durante 5 ciclos y después de
eso, los reevaluaremos.

---¡Gracias! ---dijo &amp;∗∗&amp;--- Me siento bastante optimista al respecto.

---Te recomiendo que seas cautamente optimista. La esperanza es fácil de
plantar pero difícil de cosechar.

∗∗∗ {.centrado .espacio-arriba1}

---¿Puedes darme una actualización en el planeta !=52? Dijo |}}|,
bastante satisfecho. {.espacio-arriba1 .sin-sangria}

---Bueno. La situación no es óptima. Las especies dominantes lograron
resistir los efectos del virus del monetarismo y prosperaron bajo este
durante aproximadamente 2.47 ciclos. Sin embargo, el virus mutó y
amenaza con volverse un peligro inminente para todo el planeta.
Realmente tenía grandes esperanzas para esta especie de sangre fría.

---Continúa con la limpieza del sistema, utiliza el asteroide grado
Diez. Eso será suficiente para destruir el planeta.

El mentor dejó a su alumno para que terminara la orden. Sin embargo, 
&amp;∗∗&amp; cambió el grado del asteroide a Siete. Esto no destruiría 
planeta !=52 pero tenía el poder suficiente para darle una segunda oportunidad. 
&amp;∗∗&amp; sintió la esperanza de una cosecha tardía.

# The Order of The Singularity Thinkers

# 44. The Order of The Singularity Thinkers @ignore

## By [Gerardo Horacio Porcayo](997b-semblances.xhtml#a17) {.centrado}

The group was born hermetic by context. We never intended anything but a
way to communicate. Twelve species and expeditions in orbit on Cygnus
X-1, under a sensory blur as a byproduct of the black hole emissions
that pushed us to develop these modules (kind of a deep diving
bathyscaphe, but never beyond the crescent-shaped emission ring nor the
event horizon) and to our captain vessels to abandon the near area to
avoid a major damage in the instruments.

Ours was a field research on the edge, it was like going back to old
Mercury Missions: a single pilot capsule for each race; twelve
satellites with their instruments fixed on the core of the high
radiation light donut. We theorized a similar orbit in our capsules and
found an efficient way of transmissions between us. Math was our first
language and then a strange susceptibility made us skip, leave behind
any reference to race or planet of origin. We’re linked by our study
object: it is the center of our very lives. Gradually we’ve confronted
many theories, postulates, even philosophies out of any anthropomorphism
(or any variant of it).

As we kept on this cycle, we built a common schedule: formal discussions
on the first part of the day, theological debate on the second part, and
plain science fiction speculation or adventures on the last third.

Nobody’s sure about what’s happening outside of this ring. We imagined
the construction of an interplanetary space station around the only gas
planet that’s left.

From time to time a rocket robot used to deliver supplies and spare
parts.

Three days ago, capsule five was the first to receive blue prints and
materials for an escape route. After that, we all received something
similar. We’ve conjectured there’s a war between our races in the
vicinity.

We’re not gonna take a step back. We chose to stay here, to perpetuate
this natural Order of the Singularity Thinkers. We’re gonna return the
rockets with these messages, the reunited data and all the philosophy
papers we achieve.

We trust that nothing will stop our supplies. Anyway, it would be better
to die in here than to be added to another absurd war.

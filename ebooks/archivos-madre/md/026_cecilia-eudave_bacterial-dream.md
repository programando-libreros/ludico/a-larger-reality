# Bacterial Dream

# 13. Bacterial Dream @ignore

## By [Cecilia Eudave](997b-semblances.xhtml#a09) \
Translated by [Toshiya Kamei](997b-semblances.xhtml#a53) {.centrado}

I dreamed I was in someone else’s dream, which is supposed to 
be the beginning of the end. I awoke in a sweat, wrapped in a hot 
nebula and out of breath, my gaze still dazed with remnants of my 
dreams mixed with theirs. Surely this is no ordinary infection. 
In fact, the bacteria are gaining ground. When I found out they 
were making horizontal transfers to my DNA to confuse it and resist, 
I became paranoid. How do they do it? It’s hard
to believe, but they’re hairy! They spear my poor deoxyribonucleic acid
with fine hairs as strong as Ariadne’s thread. I imagined hairs that
formed an intricate network of incredibly sensitive connections around
my brain crisscrossing my body, creating a huge cocoon to nest their
nightmares in and control me. Horrible. That’s why I signed up for a
support group that knows how to thwart bacteria. Some of its members
have been trying all kinds of antibiotics and experimental treatments
for years. I told them about my case, but they didn’t believe what the
wretched bacteria were devouring: “Who are you without your dreams? The
bacteria are evil. Go get them, show no mercy.” They recommended
phagotherapy: bacteria-eating bacteria.

Will it work? The bacteria that inhabit me are very fierce, determined
to infect me with their millennial memories until I become assimilated.
They seem like a condemned, cursed tribe, reproducing their evil ways
and their nefarious behaviors until infinity in order to linger on. They
know how to take refuge in sleep or delirium. Sucking their most
primitive and powerful vital essence from living beings, they go from
body to body and last forever. I have no idea what they saw in me rather
than another person. It’s possible they like how I dream or how they
dream about me. I’ve turned out to be a great breeding ground.

Maybe it’s not quite so bad even if I lose this battle. Maybe something
of my existence survives in a small dream scene, secluded and
distributed among all of them. Or maybe they want to remind us that
we’re, in turn, bacteria within another dying organism, always with
self-serving hunger, always dreaming that someone else dreams about us.

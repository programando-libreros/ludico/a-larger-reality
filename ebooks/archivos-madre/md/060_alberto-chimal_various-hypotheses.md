# Various Hypotheses Explain the Absence of Dinosaur Remains Immediately Beneath the Geological Signature Known as the K-Pg Boundary, Which Attests to the Chicxulub Asteroid Impact on Earth and the Mass Extinction of Terrestrial Lifeforms at the End of the Cretaceous Period. But It Still Poses This Curious Enigma, Which Could Be Described as the Apparent Absence of Dinosaurs for Years Before the Fatal Moment. Then One Wonders, “Where Were the Dinosaurs? What Were They Doing? Didn’t They Have Anything Better to Do Than Wait for Their Own Demise in the Midst of Flames and Horrific Suffering?” I Surely Wouldn’t Blame Them If They Had Something Else in Mind

# 30. Various Hypotheses Explain the Absence of Dinosaur Remains Immediately Beneath the Geological Signature Known as the K-Pg Boundary, Which Attests to the Chicxulub Asteroid Impact on Earth and the Mass Extinction of Terrestrial Lifeforms at the End of the Cretaceous Period. But It Still Poses This Curious Enigma, Which Could Be Described as the Apparent Absence of Dinosaurs for Years Before the Fatal Moment. Then One Wonders, “Where Were the Dinosaurs? What Were They Doing? Didn’t They Have Anything Better to Do Than Wait for Their Own Demise in the Midst of Flames and Horrific Suffering?” I Surely Wouldn’t Blame Them If They Had Something Else in Mind @ignore

## By [Alberto Chimal](997b-semblances.xhtml#a02) \
Translated by [Toshiya Kamei](997b-semblances.xhtml#a53) {.centrado}

1. The asteroid’s impact hid the evidence of another extinction: the
one caused by mysterious carnivorous bacteria to which
twenty-eighth-century scientists, if we don’t become extinct before,
will give the popular name “wild spirilla.” Centuries before the
asteroid struck, the spirilla were already a threat: not only did
they kill creatures with eukaryotic cells (especially the
dinosaurs), they ate their bodies, leaving nothing, not even the
bones. The asteroid killed those invisible killers, which were
burned in the waves of fire, along with almost all of the creatures
that hadn’t yet become their victims. Then, a long time later, we
came to dig the earth.
2. The asteroid’s impact hid the evidence of another extinction: the
one caused by the diamond creatures that won’t have a name in the
twenty-eight century (and if we become extinct before, less likely
still). They nested at the bottom of the darkest caverns and the
deep sea, preferring darkness and high pressures. Vast, sparkling,
hard-skinned, and with enormous strength, no smarter than sharks,
they flew up to great heights, dropped, and snatched the dinosaurs,
poor distressed lizards, and then ate them in their remote lairs.
(When the asteroid hit Chicxulub, the explosion killed almost all
the diamond creatures, but some were expelled from Earth’s
atmosphere ---their bodies were enormously strong, and they could
also enter the dormant state, wrap themselves in a hermetic cocoon,
and last for years without air or food--- and after a long trip,
they reached the interior of Jupiter’s atmosphere, even darker and
oppressive than their old home, where they are happy to this day.)
3. The asteroid’s impact was foreseen by dinosaur culture’s seers, with
large brains and fangs, and tiny arms because their scope wasn’t
that of the material world (today we call them tyrannosaurs). They
saw it coming and, as that community of intelligent species had a
lot of precognition but very little telekinesis ---or tools of any
kind to divert asteroids--- they decided to commit mass suicide.
And they did it in what, for them, was the Flower of Language
Humility day of the year 8,576,234 of the Phaminophene Calendar, and
their world’s hills and cities remained silent for centuries until
the time of ruin and fire came. {.espacio-arriba1}
4. The asteroid’s impact was predicted by an Iguanodon specialist,
Ssssumorssr del Cailano Yarepsíl, who immediately notified the
General Council of Elders, the most enlightened dinosaurs’ governing
body. With the common sense that was recurrent among them, they
discussed the possibilities available to that rational,
technological and immensely ancient civilization. They concluded the
asteroid couldn’t be stopped, but it was possible, instead, to
protect themselves from the worst consequences of the catastrophe.
Therefore, on the day Imagined Plenitude of the Creatures of the
year 82dce9 of the Crivistorno Calendar, the great project began:
the excavation of the great tunnels and the construction of
subterranean cities, where all intelligent dinosaurs could take
refuge from the ruin and fire in just another year, and where they
are happy until today because you can’t imagine the beauty of the
gardens, the painted walls, the artificial suns that illuminate
them, or the refinement and goodness of their machines, which keep
them alive and equal without taking away their dignity or uniting
them to unjust work. {.espacio-arriba1}
5. The asteroid’s impact wasn’t foreseen by anyone. No one saw it
coming because the dinosaurs weren’t smart. What nonsense to talk
about intelligent dinosaur species! Rather, centuries earlier, there
was a glow in Chicxulub, on a plain that is now submerged: a circle
of light that would have resembled the “portals” that look like ones
from Hollywood movies to anyone who has seen one, which means that
it didn’t disturb any creature. Through the portal (it was a
portal), unmanned trucks, robots, and flying drones of the meat
company entered, little by little, all with weapons at their
disposal, all obedient. And thus began a century-old, violent
massacre of all the creatures within reach, and their carcasses were
cut right there, turned into packages, placed in refrigeration to be
taken to the twenty-eighth century (in fact, it was a time
machine!), where the destruction allowed or made possible by
demagogues and oligarchs is already ancient history, but they were
already bored with slave meat. Finally having killed off the
remaining humans, they want new dishes. {.espacio-arriba1}
6. The asteroid’s impact wasn’t foreseen by anyone. Rather, centuries
earlier, there was a glow in Chicxulub, on a plain that is now
submerged: a circle of light that would have resembled the “portals”
that look like ones from Hollywood movies to anyone who has seen one
and through which entered trained shepherds, hunters, and trainers
who took away millions of creatures. They only wanted to videotape
them for the climactic scene of _Jurassic Park 143: Revenge of the
Return - Conclusion - Second Return_, which had to be more
impressive than that of _The Newest Avengers: Endgame 4 - Final
Chapter_, because it had already used up the budget assigned to
digital animation. But once in the human present, the dinosaurs
turned out to be notoriously difficult to control. They seized the
facilities that kept the time machine running and, from then on,
they devoted themselves to hunting and gobbling up those small,
fragile creatures. {.espacio-arriba1}
7. The asteroid’s impact was prevented, in fact, by the dinosaurs’
large space warfare machines, which had already defeated spiders
from Mars and the diamond beasts from Jupiter’s heart. Thus,
dinosaur cultures have survived to this day, in which their
demagogues and oligarchs dominate them through deception and hatred,
the dsungarípteros commit genocide against the pteranodontes, the
ecological catastrophe competes with the nuclear to see which occurs
first and everyone is going crazy, slowly, while their communities
dissolve into petty hatreds and useless arguments. An aragosauria
scientist called Rrrromusrrs de la Clíspera Yonalia has invented a
time machine: a space-time ship that can effortlessly navigate four
dimensions and has a desperate plan: to travel to the time before
the impact and _prevent_ the space warfare machines from doing their
work. It’ll release a cloud of wild spirilla in each one’s
atmosphere. They won’t stop those deaths. Nor the others. Let the
asteroid collide. May everything be over, because death and oblivion
are better than this. (Perhaps, when his work is done, he can rescue
some of his remote ancestors and take them to his ship, which he has
called Nothing, toward another part, another different future.) {.espacio-arriba1}

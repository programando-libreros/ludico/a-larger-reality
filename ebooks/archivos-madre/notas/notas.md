Hago aquí la misma recomendación que en la edición de 2018: si no han visto ese discurso, vayan y [vean este video](https://www.youtube.com/watch?v=Et9Nf-rsALk).

I make the same recommendation here as I did in the 2018 edition. If you have not heard that speech, go [watch this video](https://www.youtube.com/watch?v=Et9Nf-rsALk).

_Gilda_, 1946. Película de cine _noir_ dirigida por Charles Vidor e interpretada por Rita Hayworth y Glenn Ford.

The 1946 film noir _Gilda_, directed by Charles Vidor and starring Rita Hayworth and Glenn Ford.
